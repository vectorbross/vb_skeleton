#!/bin/bash
php_bin=$(which php)
composer_bin=$(which composer)
relwebroot=web
dbport=3306
cronfile=/etc/crontab
nl=$'\n'
IFS='+'
for arg in "$@"
do
  if [ "${arg:0:9}" == "--branch=" ]; then
    branch="${arg:9}"
  elif [ "${arg:0:7}" == "--root=" ]; then
    root="${arg:7}"
  elif [ "${arg:0:7}" == "--slug=" ]; then
    slug="${arg:7}"
  elif [ "${arg:0:7}" == "--host=" ]; then
    host="${arg:7}"
  elif [ "${arg:0:9}" == "--domain=" ]; then
    domain="${arg:9}"
  elif [ "${arg:0:9}" == "--dbhost=" ]; then
    dbhost="${arg:9}"
  elif [ "${arg:0:9}" == "--dbname=" ]; then
    dbname="${arg:9}"
  elif [ "${arg:0:9}" == "--dbuser=" ]; then
    dbuser="${arg:9}"
  elif [ "${arg:0:9}" == "--dbpass=" ]; then
    dbpass="${arg:9}"
  elif [ "${arg:0:7}" == "--type=" ]; then
    read -r -a types <<< "${arg:7}"
    if [ "${type[0]}" == "branch" ]; then
      host="dev"
    else
      case $branch in

        master)
          host="dev"
          ;;

        *)
          host="$branch"
          ;;

      esac
    fi
  fi
done
PROJECT_ROOT="${HOME%/}$root"
HOST_ROOT="$PROJECT_ROOT/$host"
PRIVATE_FILES="$PROJECT_ROOT/files/${host}/private"
PUBLIC_FILES="$PROJECT_ROOT/files/${host}/public"
GIT_ROOT="${HOME}/git"
#echo $PROJECT_ROOT

# params:
# 1: site directory
# 2: 0 or 1 (1 = maintenance mode)
maintenance() {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  drush state:set system.maintenance_mode "$2"
  drush cr
}

# params:
# 1: site directory
backup_site() {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  echo make backup of "$1"
  drush sql:dump --result-file="$PROJECT_ROOT/files/${host}/backups/$1.sql"
}

# params:
# 1: site directory
restore_site() {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  echo restore backup of "$1"
  drush sql:cli < "$PROJECT_ROOT/files/${host}/backups/$1.sql"
}

# params:
# 1: site directory
drush_cim () {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  config_diff=$(diff -r "$HOST_ROOT/config/$1" "$PROJECT_ROOT/files/${host}/config/$1")
  if [ "$config_diff" != "" ] || [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}cim${IFS}" ]]; then
    drush cr
    echo drush cim "$1"
    drush -y cim
    if [ $(drush pml | grep -c 'glazed') -ne 0 ]; then
      DEFAULT_THEME=$(drush cget system.theme default)
      drush eval "\Drupal::formBuilder()->submitForm('Drupal\system\Form\ThemeSettingsForm', new \Drupal\Core\Form\FormState(), '${DEFAULT_THEME:24}');"
    fi
    if [ $? -ne 0 ]; then
      echo "drush -y cim --uri=$1 failed"
      exit 1
    fi
  fi
}

# params:
# 1: site directory
drush_updb () {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  maintenance "$1" 1
  backup_site "$1"
  echo drush updb "$1"
  drush -y updb
  if [ $? -ne 0 ]; then
    echo "drush updb --uri=$1 failed"
    echo "Restoring site $1"
    restore_site "$1"
    maintenance "$1" 0
    exit 1
  fi
  drush_cim $1
  if [ $? -ne 0 ]; then
    echo "Restoring site $1"
    restore_site "$1"
    maintenance "$1" 0
    exit 1
  fi
  maintenance "$1" 0
}

# params:
# 1: site directory
down_sync() {
  cd "$HOST_ROOT/$relwebroot/sites/$1"
  echo down sync "$1"
  if [ "$host" != "production" ]; then
    drush -y rsync @server."$1"_production:%files/ @self:%files
    drush -y rsync @server."$1"_production:%private/ @self:%private
    drush -y sql:sync @server."$1"_production @self
  fi
}

# params:
# 1: site directory
deploy() {
  if [ "$host" == "dev" ]; then
    echo "Deploying $1 on dev"
    if [ "$1" != "default" ]; then
      mkdir "$PROJECT_ROOT/dev/$relwebroot/sites/$1"
    fi
    ln -s "$PUBLIC_FILES/$1" "$PROJECT_ROOT/dev/$relwebroot/sites/$1/files"
    echo "<?php$nl" > "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "\$databases['default']['default'] = array (" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'database' => '$dbname'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'username' => '$dbuser'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'password' => '$dbpass'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'prefix' => ''," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'host' => '$dbhost'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'port' => '$dbport'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\mysql'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'driver' => 'mysql'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "  'collation' => 'utf8mb4_general_ci'," >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo ");" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "\$settings['config_sync_directory'] = '../config/$1';" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "\$config['system.file']['path']['temporary'] = '../tmp';" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "\$settings['file_private_path'] = '../private/$1';" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "\$settings['trusted_host_patterns'] = array('^${domain//[.]/\\.}\$');" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    cd "$PROJECT_ROOT/$host"
    drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55)' > "$PROJECT_ROOT/files/${host}/salts/$1.txt"
    echo "\$settings['hash_salt'] = file_get_contents('$PROJECT_ROOT/files/${host}/salts/$1.txt');" >> "$PROJECT_ROOT/dev/$relwebroot/sites/$1/settings.php"
    echo "# Drupal cron ${domain}" >> "$cronfile"
    echo "$(( $RANDOM % 50 )) * * * * cd $PROJECT_ROOT/dev && drush --uri=https://${domain} cron" >> "$cronfile"
    exit 0
  fi
  if [ "$host" == "staging" ]; then
    rsync -a "$PROJECT_ROOT/files/dev/public/$1/" "$PUBLIC_FILES/$1"
    rsync -a "$PROJECT_ROOT/files/dev/private/$1/" "$PRIVATE_FILES/$1"
    if [ "$1" != "default" ]; then
      mkdir "$PROJECT_ROOT/staging/$relwebroot/sites/$1"
    fi
    ln -s "$PUBLIC_FILES/$1" "$PROJECT_ROOT/staging/$relwebroot/sites/$1/files"
    echo "<?php$nl" > "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "\$databases['default']['default'] = array (" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'database' => '$dbname'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'username' => '$dbuser'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'password' => '$dbpass'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'prefix' => ''," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'host' => '$dbhost'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'port' => '$dbport'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\mysql'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'driver' => 'mysql'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "  'collation' => 'utf8mb4_general_ci'," >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo ");" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "\$settings['config_sync_directory'] = '../config/$1';" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "\$config['system.file']['path']['temporary'] = '../tmp';" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "\$settings['file_private_path'] = '../private/$1';" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    echo "\$settings['trusted_host_patterns'] = array('^${domain//[.]/\\.}\$');" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    cd "$PROJECT_ROOT/$host"
    drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55)' > "$PROJECT_ROOT/files/${host}/salts/$1.txt"
    echo "\$settings['hash_salt'] = file_get_contents('$PROJECT_ROOT/files/${host}/salts/$1.txt');" >> "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php"
    drush sql:sync "@server.$1_dev" "@server.$1_staging"
    echo "# Drupal cron ${domain}" >> "$cronfile"
    echo "$(( $RANDOM % 50 )) * * * * cd $PROJECT_ROOT/staging && drush --uri=https://${domain} cron" >> "$cronfile"
    exit 0
  fi
  if [ "$host" == "production" ]; then
    rsync -a "$PROJECT_ROOT/files/staging/public/$1/" "$PUBLIC_FILES/$1"
    rsync -a "$PROJECT_ROOT/files/staging/private/$1/" "$PRIVATE_FILES/$1"
    if [ "$1" != "default" ]; then
      mkdir "$PROJECT_ROOT/production/$relwebroot/sites/$1"
    fi
    ln -s "$PUBLIC_FILES/$1" "$PROJECT_ROOT/production/$relwebroot/sites/$1/files"
    cp "$PROJECT_ROOT/staging/$relwebroot/sites/$1/settings.php" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -r -i "s/' => '(.*)stag(\.|')/' => '\1prod\2/g" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -r -i "s/staging\/salts/production\/salts/g" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -r -i "s/trusted_host_patterns'] = array\\('\^(.*)\$/trusted_host_patterns'] = array('^${domain//[.]/\\\\.}\$', '^www\\\\.${domain//[.]/\\\\.}\$');/g" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -i "2i if(php_sapi_name() != 'cli' && isset(\$_SERVER['SERVER_NAME']) && \$_SERVER['SERVER_NAME'] == 'www.${domain}') {" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -i "3i   header('Location: https://${domain}'.\$_SERVER['REQUEST_URI']);" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -i "4i   exit();" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    sed -i "5i }" "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    echo "\$config['vb_core.settings']['dev'] = FALSE;" >> "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    echo "\$config['vb_core.settings']['db-max'] = 1000;" >> "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    echo "\$config['vb_core.settings']['form-cache-expire'] = 600;" >> "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    echo "\$config['reroute_email.settings']['enable'] = FALSE;" >> "$PROJECT_ROOT/production/$relwebroot/sites/$1/settings.php"
    cd "$PROJECT_ROOT/$host"
    drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55)' > "$PROJECT_ROOT/files/${host}/salts/$1.txt"
    drush sql:sync "@server.$1_staging" "@server.$1_production"
    echo "# Drupal cron ${domain}" >> "$cronfile"
    echo "$(( $RANDOM % 50 )) * * * * cd $PROJECT_ROOT/production && drush --uri=https://${domain} cron" >> "$cronfile"
    echo "# Drupal scheduler ${domain}" >> "$cronfile"
    echo "* * * * * cd $PROJECT_ROOT/production && drush --uri=https://${domain} scheduler:cron" >> "$cronfile"
    echo "# database check ${domain}" >> "$cronfile"
    echo "* * * * * cd $PROJECT_ROOT/production && drush --uri=https://${domain} ev '_vb_core_cache_check()'" >> "$cronfile"
    if [ "$1" == "default" ]; then
      echo "# clear tmp files every night" >> "$cronfile"
      echo "55 $(( $RANDOM % 4 )) * * * rm -rf $PROJECT_ROOT/production/tmp && mkdir $PROJECT_ROOT/production/tmp && chmod 775 $PROJECT_ROOT/production/tmp" >> "$cronfile"
    fi
  fi
}

if [ ! -d "$HOST_ROOT" ]; then
  mkdir -p "$HOST_ROOT" "$PROJECT_ROOT/files/${host}/private/default" "$PROJECT_ROOT/files/${host}/public/default" "$PROJECT_ROOT/files/${host}/backups" "$PROJECT_ROOT/files/${host}/config" "$PROJECT_ROOT/files/${host}/salts"
  chmod 775 "$PROJECT_ROOT/files/${host}/private/default" "$PROJECT_ROOT/files/${host}/public/default"
  touch "$PROJECT_ROOT/files/${host}/composer.json" "$PROJECT_ROOT/files/${host}/composer.lock"
fi
if [ ! -d "$GIT_ROOT" ]; then
  mkdir "$GIT_ROOT"
fi
if [ ! -d "$GIT_ROOT/${slug}-$host.git" ]; then # initial deployment of $host; order will always be dev / staging / production
  if [ "$domain" == "" ]; then
    echo "Initial deployment needs to be done manually."
    exit 0
  fi
  git clone --single-branch --branch $branch --bare git@bitbucket.org:vectorbross/$slug.git "$GIT_ROOT/${slug}-$host.git"
  cd "$GIT_ROOT/${slug}-$host.git"
  GIT_WORK_TREE=$HOST_ROOT git checkout
  cd "$HOST_ROOT"
  $php_bin -d memory_limit=-1 $composer_bin install
  mkdir tmp && chmod 775 tmp
  ln -s "$PRIVATE_FILES" private
  rm -rf "$relwebroot/sites/default/files"
  deploy "default"
else
  # git pull if necessary
  if [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}git${IFS}" ]]; then
    cd "$GIT_ROOT/${slug}-$host.git"
    git_status=$(GIT_WORK_TREE="$HOST_ROOT" git status -uno --porcelain)
    if [ $? -ne 0 ] || [ ! -z "$git_status" ]; then
      echo "There are unstaged changes ($git_status). Breaking of before any git, composer of config changes are made. Ask you colleagues if they are aware of the unstaged changes and process them before continuing."
      exit 16
    fi
    if [ ! -d "$PROJECT_ROOT/files/${host}/config" ]; then
      mkdir "$PROJECT_ROOT/files/${host}/config"
    else
      rm -rf "$PROJECT_ROOT/files/${host}/config"/*
    fi
    cp -r "$HOST_ROOT"/config/* "$PROJECT_ROOT/files/${host}/config"
    echo git pull --rebase origin $branch
    if [ "$branch" == "production" ] && [ "$host" == "production" ]; then
      TARGET_ROOT="$PROJECT_ROOT/preproduction"
      rsync -a --exclude 'private' --exclude "$relwebroot/sites/*/files" "$HOST_ROOT/" "$TARGET_ROOT"
      GIT_WORK_TREE="$TARGET_ROOT" git pull --rebase origin production
      cd "$TARGET_ROOT"
      if [ ! -f private ]; then
        ln -s "$PRIVATE_FILES" private
      fi
      cd "$HOST_ROOT/$relwebroot/sites"
      for SITE in */ ; do
        if ! [ -L "${SITE%/}" ] && [ ! -f "$TARGET_ROOT/$relwebroot/sites/${SITE}files" ]; then
          chmod u+w "$TARGET_ROOT/$relwebroot/sites/${SITE}"
          ln -s "$PUBLIC_FILES/${SITE}" "$TARGET_ROOT/$relwebroot/sites/${SITE}files"
          chmod u-w "$TARGET_ROOT/$relwebroot/sites/${SITE}"
        fi
      done
    else
      TARGET_ROOT="$HOST_ROOT"
      if [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}branch${IFS}" ]] && [[ "$host" == "dev" ]]; then
        if [ -f "$PROJECT_ROOT/files/${host}/branch.lock" ]; then
          echo "Dev is locked. No new other branch may be deployed. Check with your colleagues when dev can be unlocked. Remove the lock (files/dev/branch.lock) when it is OK."
          exit 8
        fi
        current_branch=$(GIT_WORK_TREE="$HOST_ROOT" git rev-parse --abbrev-ref HEAD)
        cd "$HOST_ROOT/$relwebroot/sites"
        for SITE in */ ; do
          drush sql:dump --result-file="$PROJECT_ROOT/files/${host}/backups/${SITE%/}-${current_branch}.sql"
        done
        cd "$GIT_ROOT/${slug}-$host.git"
        if [ "$branch" != "$current_branch" ]; then
          GIT_WORK_TREE="$HOST_ROOT" git fetch
          GIT_WORK_TREE="$HOST_ROOT" git switch $branch
          GIT_WORK_TREE="$HOST_ROOT" git checkout -f $branch
          cd "$HOST_ROOT/$relwebroot/sites"
          for SITE in */ ; do
            if [ -f "$PROJECT_ROOT/files/${host}/backups/${SITE%/}-${branch}.sql" ]; then
              drush sql:cli < "$PROJECT_ROOT/files/${host}/backups/${SITE%/}-${branch}.sql"
            fi
          done
          cd "$GIT_ROOT/${slug}-$host.git"
        fi
        if [ "$branch" != "default" ]; then
          touch "$PROJECT_ROOT/files/${host}/branch.lock"
        fi
      fi
      echo "GIT_WORK_TREE=$HOST_ROOT git pull --rebase origin $branch"
      GIT_WORK_TREE="$HOST_ROOT" git pull --rebase origin $branch
      if [ $? -ne 0 ]; then
        exit 4
      fi
    fi
  else
    TARGET_ROOT="$HOST_ROOT"
  fi

  cd "$TARGET_ROOT"

  # composer install if necessary
  if [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}composer${IFS}" ]]; then
    composer_json_diff=$(diff "$TARGET_ROOT/composer.json" "$PROJECT_ROOT/files/${host}/composer.json")
    composer_lock_diff=$(diff "$TARGET_ROOT/composer.lock" "$PROJECT_ROOT/files/${host}/composer.lock")
    if [ "$composer_json_diff" != "" ] || [ "$composer_lock_diff" != "" ]; then
      echo composer install
      chmod u+w "$TARGET_ROOT/$relwebroot/sites/default"
      $php_bin -d memory_limit=-1 $composer_bin install
      if [ "$host" != "production" ]; then
        cp "$PROJECT_ROOT/$host/etc/environments/robots.txt.dev" "$PROJECT_ROOT/$host/$relwebroot/robots.txt"
      fi
      chmod u-w "$TARGET_ROOT/$relwebroot/sites/default"
      cp "$TARGET_ROOT/composer.json" "$PROJECT_ROOT/files/${host}/composer.json"
      cp "$TARGET_ROOT/composer.lock" "$PROJECT_ROOT/files/${host}/composer.lock"
    fi
    if [ "$branch" == "production" ] && [ "$host" == "production" ]; then
      if [ -d "$PROJECT_ROOT/postproduction" ]; then
        chmod u+w "$PROJECT_ROOT/postproduction/$relwebroot/sites"/*
        rm "$PROJECT_ROOT/postproduction/private"
        rm "$PROJECT_ROOT/postproduction/$relwebroot/sites"/*/files
        rm -rf "$PROJECT_ROOT/postproduction"
      fi
      mv "$HOST_ROOT" "$PROJECT_ROOT/postproduction" && mv "$TARGET_ROOT" "$HOST_ROOT"
    fi
  fi

  # drush updb if necessary
  if [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}updb${IFS}" ]]; then
    cd "$HOST_ROOT/$relwebroot/sites"
    has_errors=0
    for SITE in */ ; do
      drush_updb ${SITE%/}
      if [ $? -ne 0 ]; then
        has_errors=1
      fi
      if [ $has_errors -ne 0 ]; then
        exit 2
      fi
    done

  fi

  # drush cim if necessary
  if [[ "${IFS}${types[*]}${IFS}" =~ "${IFS}cim${IFS}" ]]; then
    cd "$HOST_ROOT/$relwebroot/sites"
    has_errors=0
    for SITE in */ ; do
      drush_cim ${SITE%/}
      if [ $? -ne 0 ]; then
        has_errors=1
      fi
      if [ $has_errors -ne 0 ]; then
        exit 1
      fi
    done
  fi
fi
