#!/bin/sh
# Install updated packages, if any.
composer install

# Update site
vendor/bin/drush updb -y

# Clear caches
vendor/bin/drush cr

# Import configuration overrides if there is any config yet.
vendor/bin/drupal config:import:single --file=../config/default/config_ignore.settings.yml
vendor/bin/drupal config:import:single --file=../config/default/config_split.config_split.development.yml

# Clear caches
vendor/bin/drush cr

# Import all config for this environment
vendor/bin/drush cim -y

# Clear caches
vendor/bin/drush cr

# Update translations
#vendor/bin/drush locale-check
#vendor/bin/drush locale-update

# Run cron
vendor/bin/drush cron

# Clear caches
vendor/bin/drush cr
