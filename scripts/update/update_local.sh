#!/bin/sh
# To execute from the root of the project.
# It is advised to disable xdebug while running this script to speed up the process.

# Install updated packages, if any.
composer install

# Update site
drush updb -y

# Clear caches
drush cr

# Import configuration overrides if there is any config yet.
drupal config:import:single --file=../config/default/config_ignore.settings.yml
drupal config:import:single --file=../config/default/config_split.config_split.local.yml

# Clear caches
drush cr

# Import all config for this environment
drush cim -y

# Clear caches
drush cr

# Update translations
drush locale-check
drush locale-update

# Run cron
drush cron

# Clear caches
drush cr
