<?php

define('VB_PROJECT_NAME', FALSE);

// Drupal 9 drush 8 "support"
if (!defined('CONFIG_SYNC_DIRECTORY')) {
  define('CONFIG_SYNC_DIRECTORY', 'default');
}
if (!defined('CONFIG_ACTIVE_DIRECTORY')) {
  define('CONFIG_ACTIVE_DIRECTORY', 'active');
}

// Ignore front-end folders.
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Setup config directory.
$settings['config_sync_directory'] = '../config/default';
// Drupal 9 drush 8 "support"
global $config_directories;
$config_directories['sync'] = '../config/default';

// Setup private files folder.
$settings['file_private_path'] = '../private/files';

// Load specific environment services file.
if (file_exists(DRUPAL_ROOT . '/sites/environments/' . VB_PROJECT_ENVIRONMENT . '.services.yml')) {
  $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/environments/' . VB_PROJECT_ENVIRONMENT . '.services.yml';
}
