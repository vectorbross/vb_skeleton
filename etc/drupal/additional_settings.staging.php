<?php

/**
 * @file
 * The additional settings for the staging environment.
 */

define('VB_PROJECT_ENVIRONMENT', 'staging');

include_once DRUPAL_ROOT . '/../etc/drupal/general.settings.php';

// Configure config split directory.
$config['config_split.config_split.whitelist']['status'] = TRUE;
$config['config_split.config_split.blacklist']['status'] = TRUE;
$config['config_split.config_split.local']['status'] = FALSE;
$config['config_split.config_split.dev']['status'] = FALSE;
$config['config_split.config_split.staging']['status'] = TRUE;
$config['config_split.config_split.production']['status'] = FALSE;
