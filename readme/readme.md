In this folder we can add some readme-info and best-practice information on how to install and 
use the skeleton and profile.

Install from Skeleton:

composer create-project vectorbross/vb_skeleton [project_name] --no-dev --no-interaction

OR

git clone git@bitbucket.org:vectorbross/vb_skeleton.git [project_name]
cd [project_name]
rm -rf .git
composer install
